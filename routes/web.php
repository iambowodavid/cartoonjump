<?php

Auth::routes();

//general routes
Route::get('/', function(){ return view('index'); });
Route::get('/home', function(){ return redirect()->away('/'); });
Route::get('/media/play/{mediaID}', 'Media\Play@index');

//user routes
Route::get('/user/dashboard/', 'User\Profile@index');
Route::get('/user/dashboard/update/profile/{id}', 'User\Profile@edit');
Route::post('/user/dashboard/update/profile/{id}', 'User\Profile@update');

//Route::get('/home', function(){ return redirect()->away('/admin/'); });
//Route::get('/register', function(){ return redirect()->away('/admin/'); })->name('register');
//Route::get('/', function(){ return redirect()->away('/admin/'); });
//Route::get('/admin', function(){ return redirect()->away('/admin/dashboard'); });

/*Route::get('/setup', 'Setup@index');
Route::get('/admin/dashboard', 'Admin\Dashboard@index');
Route::get('/admin/manage/profile/{id}', 'Admin\ManageProfile@new');
Route::get('/admin/delete/profile/{id}', 'Admin\ManageProfile@delete');
Route::post('/admin/manage/profile/{id}', 'Admin\ManageProfile@updateOrCreateProfile');*/