<footer style="background: black;">
        <div class="container-fluid">
            <div class="col-md-12">
            <p>
                    <b style="color: ;">Copyright &copy; {{date("Y")}} Cartoon Jump.</b>
                    <br/><a href="#" style="color:white; text-transform: initial;">Privacy</a> | <a href="#" style="color:white; text-transform: initial;">Contact Us</a> | <a href="#" style="color:white; text-transform: initial;">Help Center</a> | <a href="#" style="color:white; text-transform: initial;">Terms of Use</a>
                </p>
            </div>
        </div>
    </footer>

    <section class="overlay-menu">
        <div class="container">
            <div class="row">
                <div class="main-menu">
                    <ul>
                        <li>
                            <a href="index.html">Home - Full-width</a>
                        </li>
                        <li>
                            <a href="masonry.html">Home - Masonry</a>
                        </li>
                        <li>
                            <a href="grid.html">Home - Small-width</a>
                        </li>
                        <li>
                            <a href="about.html">About Us</a>
                        </li>
                        <li>
                            <a href="blog.html">Blog Entries</a>
                        </li>
                        <li>
                            <a href="single-post.html">Single Post</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
    window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')
    </script>

    <script src="js/vendor/bootstrap.min.js"></script>

    <!--<script src="js/plugins.js"></script>-->
    <script src="js/main.js"></script>

