<div class="top-bar">
    <!-- logo-->
    <div class="logo-container">
        <img src="/img/logo-white.png">
    </div>
    <!--/logo-->

    <!--desktop nav-->
    <span class="desktop-nav">
        <a class="white" href="/"> &bull; Home</a>
        <a class="white" href="#"> &bull; Movies</a>
        <a class="white" href="#"> &bull; Series</a>
    </span>
    <!--desktop nav-->

    <!--auth nav-->
    <span class="auth-nav">
        @guest
          <a class="yellow" href="/login">Sign In</a> <b>|</b> <a class="white" href="/register">Join</a>
        @endguest

        @php $user = \Auth::user(); @endphp
        @if($user !== null && !$user->is_admin)
          <a class="white" href="/user/dashboard"><b>My Account</b> ( {{$user->first_name}} )</a>
        @endif

        @if(\Auth::user() !== null && \Auth::user()->is_admin)
          <a class="white" href="/admin/dashboard">Admin Dashboard</a>
        @endif
    </span>
    <!--auth nav-->
</div>