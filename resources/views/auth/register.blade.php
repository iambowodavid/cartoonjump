<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Cartoon Jump</title>

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/fontAwesome.css">
    <link rel="stylesheet" href="css/light-box.css">
    <link rel="stylesheet" href="css/templatemo-style.css">
    <link rel="stylesheet" href="css/custom.css">

    <link href="https://fonts.googleapis.com/css?family=Kanit:100,200,300,400,500,600,700,800,900" rel="stylesheet">

    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>

<body>
    <nav>
        <div class="logo">
            <!--<a href="index.html">High<em>way</em></a>-->
        </div>
        <div class="menu-icon">
            <span></span>
        </div>
    </nav>

    <div id="video-container" style="height: 100%;">

        @include("nav")

        <div class="video-overlay"></div>

        <div class="video-content">
            <div class="col-md-6" style="float:right;">
                <div class="modal-body" style="padding-top: 100px; min-width: 360px;">
                    <!--<h3 class="white">Signin</h3>-->
                    <form method="POST" action="{{ route('register') }}" id="register-form">
                        @csrf
                        <div class="row">
                        <div class="col-md-12">
                                <fieldset>
                                    @if ($errors->has('first_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                    <input class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                                        name="first_name" type="text" class="form-control" id="first_name" value="{{ old('first_name') }}"
                                        placeholder="First Name" required="">
                                </fieldset>
                            </div>
                            <div class="col-md-12">
                                <fieldset>
                                    @if ($errors->has('last_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                    <input class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}"
                                        name="last_name" type="text" class="form-control" id="last_name" value="{{ old('last_name') }}"
                                        placeholder="Last Name" required="">
                                </fieldset>
                            </div>
                            <div class="col-md-12">
                                <fieldset>
                                    @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                    <input name="email" type="email" class="form-control" id="email" placeholder="Email"
                                    class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}"
                                        required="">
                                </fieldset>
                            </div>
                            <div class="col-md-12">
                                <fieldset>
                                    @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                    <input name="password" type="password" class="form-control" id="password"
                                    class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                        placeholder="Password" required="">
                                </fieldset>
                            </div>
                            <div class="col-md-12">
                                <fieldset>
                                    <input name="password_confirmation" type="password" class="form-control" id="password_confirmation"
                                        placeholder="Retype Password" required="">
                                </fieldset>
                            </div>
                            <div class="col-md-12">
                                <fieldset>
                                    <input type="submit" class="btn btn-auth" value="Sign Up" />
                                </fieldset>
                            </div>
                            <div class="register-forgot">
                                Already have an account? <a href="/login" class="yellow">Login</a>
                                <!--&nbsp;&nbsp;
                                | &nbsp;&nbsp;<a href="#" class="white">Join with <img width="150px" height="40px"
                                        src="/img/facebook-btn.png" /></a>-->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <img src="/img/thewalkingdead.jpg" style="width: 100%;height:100%;">
    </div>


    @include("general_footer")
</body>

</html>