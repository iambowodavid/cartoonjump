@include("general_header")

    <div id="video-container">

        @include("nav")

        <div class="video-overlay"></div>
        <div class="video-content">
            <div class="inner" style="padding-bottom: 200px;">
                <div class="header-featured">
                    <a id="featured-href" class="scrollTo" style="text-decoration: none;" href="#">
                        <h2 id="featured-time"></h2>
                        <h6 id="featured-title"></h6>
                        <p id="featured-description"></p>
                        <div class="scroll-icon" style="text-align: center;">
                            <i class="fa fa-play-circle-o" style="font-size: 50px;"></i>
                        </div>
                    </a>
                </div>

                <a style="position: absolute; left:5px; font-size: 30px;" href="#"
                    onclick="previousVideo(); return false;"><i class="fa fa-chevron-left"></i></a>
                <a style="position: absolute; right:5px; font-size: 30px;" href="#"
                    onclick="nextVideo(); return false;"><i class="fa fa-chevron-right"></i></a>

            </div>

        </div>
        <video class="header-video" autoplay="" loop="" muted>
            <source src="video/sango.mp4" type="video/mp4" />
            <hidden class="time" style="display: none;">This Week</hidden>
            <hidden class="title" style="display: none;">Sango</hidden>
            <hidden class="description" style="display: none;">Sango, an exciting 3D animated short story movie for you
            </hidden>
            <hidden class="href" style="display: none;">/media/play/sango-full</hidden>
        </video>
        <video class="header-video" autoplay="" loop="" muted style="display: none;">
            <source src="video/highway.mp4" type="video/mp4" />
            <hidden class="time" style="display: none;">Last Week</hidden>
            <hidden class="title" style="display: none;">Highway</hidden>
            <hidden class="description" style="display: none;">Highway Super cool test demo short film</hidden>
            <hidden class="href" style="display: none;">/media/play/highway</hidden>
        </video>
        <video class="header-video" autoplay="" loop="" muted style="display: none;">
            <source src="video/robinsoncrusoe.mp4" type="video/mp4" />
            <hidden class="time" style="display: none;">April</hidden>
            <hidden class="title" style="display: none;">Robinson Crusoe</hidden>
            <hidden class="description" style="display: none;">Robinson Crusoe funny 3D animated short film</hidden>
            <hidden class="href" style="display: none;">/media/play/robinsoncrusoe</hidden>
        </video>

        <script>
        var featured = document.getElementsByClassName("header-video");
        var index = 0;
        var length = featured.length;

        populate(0);

        function nextVideo() {
            if (index + 1 < length) {
                populate(++index);
            } else {
                populate(0);
                index = 0;
            }
        }

        function previousVideo() {
            if (index - 1 >= 0) {
                populate(--index);
            } else {
                populate(length - 1);
                index = length - 1;
            }
        }

        function populate(I) {
            //getting and setting href attribute
            document.getElementById("featured-href").href = featured[I].getElementsByClassName("href")[0].innerHTML;

            //getting and setting time
            document.getElementById("featured-time").textContent = featured[I].getElementsByClassName("time")[0]
                .innerHTML;

            //getting and setting title
            document.getElementById("featured-title").textContent = featured[I].getElementsByClassName("title")[0]
                .innerHTML;

            //getting and setting description
            document.getElementById("featured-description").textContent = featured[I].getElementsByClassName(
                "description")[0].innerHTML;

            //clearing all videos
            for (var i = 0; i < length; i++) {
                featured[i].style.display = "none";
            }

            //getting and setting video
            featured[I].style.display = "inline";
        }
        </script>
    </div>

    <div class="full-screen-portfolio" id="portfolio">
        @include("media.videos")
    </div>


    @include("general_footer")
</body>

</html>