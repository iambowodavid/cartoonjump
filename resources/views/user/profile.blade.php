@extends('layouts.app')

@section('content')

<div class="container-fluid dashboard-container">

    @if (session('status'))
        <div class="alert alert-success" align="center">
            {{ session('status') }}
        </div>
    @endif
  
    <div class ="row">

        @include('user.sidebar')

         <div class ="col-md-10 dashboard-body">

            <div class ="row">

              <div class ="col-md-1">


              </div>

              <div class ="col-md-10">
                              
                <div class ="row" style="margin: 15px"> 
                              
                    <div class ="col-md-12">

                    <div class ="profile-header">

                           
                           {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}

                    </div>

                     <div class ="dashboard-content-section" style ="height: auto; padding: 5%;">
                                         
                        

                        <div class ="profile-details">

                            <div class ="row">

                                <div class ="col-md-6">

                                    <div><i class="fa fa-envelope" style ="color: #42beef; padding-left: 0px; font-size: 16px;"></i> Email Address</div>

                                    <p>{{ Auth::user()->email }}</p>

                                </div>

                                <div class ="col-md-6">

                                    <div><i class="fa fa-user" style ="color: #42beef; padding-left: 0px; font-size: 16px;"></i> First Name</div>

                                   <p>{{ Auth::user()->first_name }}</p>

                                </div>

                            </div>

                            <div class ="row">

                                <div class ="col-md-6">

                                    <div> <i class="fa fa-bookmark" style ="color: #42beef; padding-left: 0px; font-size: 16px;"></i> Last Name</div>

                                     <p> {{ Auth::user()->last_name }}</p>

                                </div>

                                <div class ="col-md-6">

                                    <div> <i class="fa fa-bookmark" style ="color: #42beef; padding-left: 0px; font-size: 16px;"></i> Registered</div>

                                     <p> {{ Auth::user()->created_at }}</p>

                                </div>

                            </div>




                        </div>
                                    
                    </div>
                </div>
                               

                </div>

              </div>


              <div class ="col-md-1">


              </div>

                </div>




         </div>

    </div>



</div>
@endsection
