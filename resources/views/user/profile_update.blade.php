@extends('layouts.app')


@section('content')
@php $user = \Auth::user(); @endphp
<div class="container-fluid dashboard-container">

    <div class="row">

        @include('user.sidebar')

        <div class="col-md-10 col-sm-10 col-xs-10 dashboard-body">

            <div class="row" style="margin: 15px">

                <div class="col-md-1">


                </div>

                <div class="col-md-10">


                    <form method="POST" action="" id="update-profile-form">
                        <div class="row">
                            <div class="col-md-12">

                                    @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                    @endif

                                    @csrf <br/><br/>

                                <div class="dashboard-content-section bluetop-border"
                                    style="height: auto; padding: 20px 10px; margin-bottom: 40px">

                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="first_name"
                                                class="col-md-12 col-form-label text-md-left">{{ __('First Name') }}</label>

                                            <div class="col-md-12">
                                                <input id="first_name" type="text"
                                                    class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                                                    name="first_name" value="{{$user->first_name}}" required autofocus>

                                                @if ($errors->has('first_name'))
                                                <span class="invalid-feedback">
                                                    <strong>{{ $errors->first('first_name') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="form-group col-md-6">
                                            <label for="last_name"
                                                class="col-md-12 col-form-label text-md-left">{{ __('Last Name') }}</label>

                                            <div class="col-md-12">
                                                <input id="last_name" type="text"
                                                    class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}"
                                                    name="last_name" value="{{$user->last_name}}" required autofocus>

                                                @if ($errors->has('last_name'))
                                                <span class="invalid-feedback">
                                                    <strong>{{ $errors->first('last_name') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>





                                </div>

                                <div class="dashboard-content-section bluetop-border"
                                    style="height: auto; padding: 20px 10px; margin-bottom: 40px">


                                    <!--<div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="password"
                                                class="col-md-12 col-form-label text-md-left">{{ __('Password') }}</label>

                                            <div class="col-md-12">
                                                <input id="password" type="password"
                                                    class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                    name="password">

                                                @if ($errors->has('password'))
                                                <span class="invalid-feedback">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="password-confirm"
                                                class="col-md-12 col-form-label text-md-left">{{ __('Confirm Password') }}</label>

                                            <div class="col-md-12">
                                                <input id="password-confirm" type="password" class="form-control"
                                                    name="password_confirmation">
                                            </div>
                                        </div>
                                    </div>-->



                                    <div class="form-group row mb-0">
                                        <div class="form-submit">
                                            <button type="submit" class="btn btn-primary">
                                                Update
                                            </button>

                                        </div>

                                    </div>






                                </div>


                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        @stop