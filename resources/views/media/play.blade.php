@include("general_header")

<div id="video-container">

    @include("nav")

    <video autoplay="" controls controlsList="nodownload" src="/video/{{$mediaID}}.mp4"
        style="width:99%; height:700px; object-fit: fill;"></video>

</div>

<div class="full-screen-portfolio" id="portfolio">
    @include("media.videos")
</div>



@include("general_footer")
</body>

</html>