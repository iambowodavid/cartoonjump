<h4 style="color:white; padding-left:10px;">Most Viewed</h4>
    <div class="container-fluid">

        <?php $files = array_values( array_diff(scandir("./img/thumbnails/"), array('..', '.')) ); natsort($files);
        foreach($files as $filename){?>
        <div class="col-md-4 col-sm-6">
            <div class="portfolio-item">
                <a href="#" data-lightbox="image-1">
                    <div class="thumb">
                        <div class="hover-effect">
                            <div class="hover-content">
                                <!--<h1>Geekaweb <em>squid</em></h1>
                                <p>Awesome Subtittle Goes Here</p>-->
                            </div>
                        </div>
                        <div class="image">
                            <img src="/img/thumbnails/<?php echo $filename;?>">
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <?php }?>

    </div>