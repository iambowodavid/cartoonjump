@extends('layouts.app')

@section('content')

<div class="container-fluid dashboard-container">

    @if (session('status'))
    <div class="alert alert-success" align="center">
        {{ session('status') }}
    </div>
    @endif

    <div class ="row">

        @include('admin.sidebar')

        <div class ="col-md-10 dashboard-body">

            <p class ="form-heading">Create New Profile</p>

            <div class="card-body">
                <form method="POST" action="" id ="register-form">
                    @csrf

                    <input type='hidden' name='proposed-id' value='{{ $profile->id }}'>

                    <div class ="row">
                        @foreach (\App\Profile::FILLABLE_CLONE as $field)
                        @php $display = str_replace("_", " ", $field); @endphp

                        <div class="form-group col-md-6">
                            <label for="{{$field}}" class="col-md-12 col-form-label text-md-left">{{ $display }}</label>

                            <div class="col-md-12">
                                <input id="{{$field}}" type="text" class="form-control{{ $errors->has($field) ? ' is-invalid' : '' }}" name="{{$field}}" value="{{ !empty(old($field)) ? old($field) : $profile->$field }}" requiredx autofocus>

                                @if ($errors->has($field))
                                <span class="invalid-feedback">
                                    <strong>{{ str_replace("_", " ", $errors->first($field) ) }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <div class="form-group row mb-0">
                        <div class="form-submit">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Save') }}
                            </button>
                            <b style='padding-left: 20px;'></b>
                            <button type="cancel" onclick=' window.location="/admin"; return false;' class="btn btn-primary">
                                {{ __('Cancel') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

</div>
@endsection
