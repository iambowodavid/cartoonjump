@extends('layouts.app')

@section('content')

<div class="container-fluid dashboard-container">

    @if (session('status'))
        <div class="alert alert-success" align="center">
            {{ session('status') }}
        </div>
    @endif
	
    <div class ="row">

        @include('admin.sidebar')

         <div class ="col-md-10 dashboard-body">

                <div class ="row">

                         <div class ="col-md-1">


                         </div>

                         <div class ="col-md-10">
                              
                           <div class ="row"> 
                              
                              <div class ="col-md-12">

                                  <div class ="dashboard-content-section" style ="height: auto; padding: 15px; margin: 0px 0px 30px 0px; display: flex; justify-content: space-between;">

                                    <div class ="heading" style="padding-top: 6px">All Profiles</div>
                                    

                                    <!--<div><input type="text" class="form-control" id="search-users" onkeyup="searchUsers()" placeholder="Enter Username"></div>-->


                                  </div>

                                  <div class ="dashboard-content-section" style ="height: auto; padding: 0px">
                                   <div class ="table-responsive">
                                    <table class="table" id ="users-table">
                                   
                                     <thead>
                                       <tr>
                                          <th>Surname</th>
                                          <th>Other Names</th>
                                          <th>City</th>
                                          <th>Email Address</th>
                                          <th>Mobile Number</th>
                                          <th>Sex</th>
                                          
                                       </tr>
                                     </thead>
                                     <tbody>
                                        @foreach ($profiles as $profile)
                                         <tr>
                                           <td>{{ $profile->surname }}</td>
                                           <td>{{ $profile->other_names }}</td>
                                           <td>{{ $profile->city }}</td>
                                           <td>{{ $profile->email }}</td>
                                           <td>{{ $profile->mobile_number }}</td>
                                           <td>{{ $profile->sex }}</td>
                                           <!--<td></td>
                                           <td></td> -->
                                           <td><a href = '{{ url("/admin/manage/profile/$profile->id") }}' ><i class="fa fa-pencil text-info"></i></a></td>
                                           <td><a href = '#' onclick="if(confirm('Are you sure you want to delete {{ $profile->surname.' '.$profile->other_names }}?')) {window.location = '{{ url("/admin/delete/profile/$profile->id") }}';} else {};"><i class="fa fa-trash text-danger"></i></a></td>
                                           
                                          </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                  </div>
                                    </div>
                               </div>
                               

                            </div>

                         </div>


                         <div class ="col-md-1">


                         </div>
                </div>
                         
                <p align='center' style='padding-top: 30px;'>
                    <a href="{{ url('admin/manage/profile/-1') }}">
                          <button class="btn btn-primary">Create New Profile</button>
                    </a>
                </p>
         </div>

    </div>
    
</div>
@endsection
