<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DB;
use Hash;
use \App\User;

class Setup extends Controller
{
    public function index()
	{
        self::defaultAdmin();
    }
	
	private static function defaultAdmin()
    {
		$admins = User::where('is_admin', '1')->get();
		
		if($admins->count() == 0)
		{
			$user = User::create([
            'name' => "admin",
            'email' => "admin@cartoonjump.com",
			'first_name' => "_",
			'last_name' => "_",
			'referral_id' => "",
            'password' => Hash::make("admin"),
          ]);
		  
		  $user->verified = 1;
		  $user->is_admin = 1;		  
		  $user->save();
		}
    }
	
	private static function wipeTable($tableName)
    {
		DB::delete("TRUNCATE TABLE $tableName");
	}
}
