<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use \App\User;

class Profile extends Controller
{
    public function __construct()
    {
        $this->middleware('user');
    }
     
    public function index()
    {
        $user = \Auth::user();
        
        return view('user.profile');
    }
    
    public function edit($id)
    {
        $user = \Auth::user($id);
       
        return view('user.profile_update')->with(['user'=>$user]);
    }
    
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        
        $validator = Validator::make($request->all(), [
            'password' => 'string|min:6|confirmed'
       ]);
		
		if($validator->fails())
		{
			return view('user.profile_update')->withErrors($validator);
		}
        
        $user->update($request->all());
        
        return redirect()->back()->with("status", "Profile Updated successfully.");
    }
}
