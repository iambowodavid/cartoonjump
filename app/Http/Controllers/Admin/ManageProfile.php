<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use App\Profile;

class ManageProfile extends Controller
{
    public function __construct()
    {
		$this->middleware('admin');
    }

    public function new($id)
    {
        $profile = Profile::findOrNew($id);

        return view('admin.manage_profile', ['profile'=>$profile]);
    }

    public function updateOrCreateProfile(Request $request)
    {
        $this->validate($request,
        [
            //'other_names' => "required|numeric|between:30,55",
        ]);

        $raw = $request->all();

        foreach($raw as $key => $value)
        {
            if($value == null)
            {
                $raw[$key] = "";
            }
        }

        $profile = Profile::find($request->input('proposed-id'));

        if($profile != null)
        {
            $profile->update($raw);
        }
        else
        {
            Profile::create($raw);
        }

        return redirect('/admin/dashboard')->with('status', 'Profile saved successfully.');
    }

    public function delete($id)
    {
        DB::delete('delete from profiles where id = ?',[$id]);

	    return redirect()->back()->with('status', 'Profile deleted successfully.');
    }
}