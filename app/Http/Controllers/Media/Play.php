<?php

namespace App\Http\Controllers\Media;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\User;

class Play extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
     
    public function index($mediaID)
    {
        return view('media.play')->with(["mediaID"=>$mediaID]);
    }
}
