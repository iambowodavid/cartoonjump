<?php

namespace App\Http\Controllers\Auth;

use DB;
use Mail;
use App\Mail\VerifyMail;
use App\User;
use App\RetonarMail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
		Validator::extend('no_space', function($attr, $value){
			return preg_match('/^\S*$/u', $value);
		});
			
        return Validator::make($data, [
            //'name' => 'required|no_space|unique:users|string|max:255',
			'first_name' => 'required|string',
			'last_name' => 'required|string',
			'phone_number' => 'min:11|numeric',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'email' => $data['email'],
			'first_name' => $data['first_name'],
			'last_name' => $data['last_name'],
            'password' => Hash::make($data['password']),
        ]);
		
		return $user;
    }
	
	public function verifyUser($token)
    {
        $email = base64_decode($token);
        
		$user = User::where('email', $email)->firstOrFail();
		
		if( isset($user) ){
            if(!$user->verified) {
                $user->verified = 1;
                $user->save();
                $status = "Your e-mail is verified. You can now login.";
            }else{
                $status = "Your e-mail is already verified. You can now login.";
            }
        }else{
            return redirect('/login')->with('warning', "Sorry your email cannot be identified.");
        }
 
        return redirect('/login')->with('status', $status);
    }
	
	protected function registered(Request $request, $user)
    {
		return redirect('/');
		
	    //$vlink = url('user/verify', base64_encode($user->email));
		
		//RetonarMail::sendWithLink($user->email, "Account Activation Required", "Welcome to Retonar Investments. \n\n Please follow the verification link below, to activate your newly created account.", $vlink);
		
        //$this->guard()->logout();
		
        //return redirect('/login')->with('status', 'We sent you an activation code. Check your email or SPAM box and click on the link to verify.');
    }
}
